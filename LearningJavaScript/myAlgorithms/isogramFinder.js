	//Checks whether a word is an Isogram (No letter is repeated in the word)

	test('isIsogram("menu") should return true', function(assert) {
      var result = isIsogram('menu'); // expect true
      var expected = true;
      assert.deepEqual(result, expected);
    });
    
    test('isIsogram("mama") should return false', function(assert) {
      var result = isIsogram('mama'); // expect false
      var expected = false;
      assert.deepEqual(result, expected);
    });

    test('isIsogram("Mame") should return false', function(assert) {
      var result = isIsogram('Mame'); // expect false
      var expected = false;
      assert.deepEqual(result, expected);
    });
    
    test('isIsogram("ebonyi") should return true', function(assert) {
      var result = isIsogram('ebonyi'); // expect true
      var expected = true;
      assert.deepEqual(result, expected);
    });
// The performance 0f the algorithm is O(n^2)
// The algorithm works with a nested loop counting the number of times same letter is encountered.
    function isIsogram(word){
      var word = word.toLowerCase();
      word = word.split("");
      var count = 0;
      for (x in word) {
        for (i in word){
          if(word[x] === word[i]){
            count++;
          }
        }
      }
      var result = count > word.length? false : true;
      return result;
    }

// The performance of the algorithm below is O(n)
/* The algorithm works by looping through and storing only an instance of an element in the array "arr"
and comparing the length of the original word array with array arr.*/
function isIsogram(word){
      var word = word.toLowerCase();
      word = word.split("");
      var arr = [];
      for (x in word){
        if (arr.indexOf(word[x]) === -1){
          arr.push(word[x]);
        }
      }
      var result = arr.length === word.length? true : false;
      return result;
    }
