//IS ISOGRAM
// The performance of the algorithm below is O(n)
/* The algorithm works by looping through and storing only an instance of an element in the array "arr".
It then compares the length of the original word array with array arr.*/
function isIsogram(word){
  var result;
  if(word === null || word === undefined || word === ""){
    result = false;
  }
  else{
    var word = word.toLowerCase();
        word = word.split("");
        var arr = [];
        for (x in word){
          if (arr.indexOf(word[x]) === -1){
            arr.push(word[x]);
          }
        }
    result = arr.length === word.length? true : false;
  }
  return result;
}


//SHOPPING CART


class ShoppingCart{
  constructor(){
    this.total = 0;
    this.items = {};
  }
  
  addItem(itemName, quantity, price){
    this.total += quantity*price;
    this.items[itemName] = quantity;
  }
  
  removeItem(itemName, quantity, price){
    if(this.items[itemName] === quantity){
      delete this.items[itemName];
    }
    else{
      this.total -= quantity*price;
      this.items[itemName] = this.items[itemName] - quantity;
    }
  }
  
  checkout(cashPaid){
    let balance;
    if(cashPaid < this.total){
      balance = "Cash paid not enough";
    }
    else{
       balance = cashPaid - this.total;
    }
    return balance;
  }
}

class Shop extends ShoppingCart{
  constructor(){
    super();
    this.quantity = 100;
  }
  
  removeItem(){
    this.quantity -= 1;
  }
}


//REMOVE DUPLICATES


function removeDuplicates(word){
  let result = {};
  //This function returns only English alphabets in word.
  function removeSpecialChar(word){
    let re = /[a-z]/g;
    word = word.toLowerCase();
  word = word.match(re);
    return word;
  }
   
   // This function takes the word and returns letters free from duplicates. 
  function removeDups(word){
    var arr = [];
    for (var x in word){
      if (arr.indexOf(word[x]) === -1){//checks if an element already in word is in arr
      arr.push(word[x]);
      }
    }
    return arr;
  }
  
  var noChar = removeSpecialChar(word)
  var cleanWord = removeDups(noChar);
  result.uniques = cleanWord.sort().join("");
  result.duplicates = noChar.join("").length - cleanWord.join("").length;
   
  return result;
}



//MY SORT


function mySort(nums) {
  let result = [];
  let odds = [];
  let evens = [];
  for (x in nums){
    if(Math.floor(nums[x])%2 === 0){
      nums[x] = Math.floor(nums[x]);
      evens.push(nums[x]);
    }
    else if(Math.floor(nums[x])%2 !== 0 && typeof nums[x] === "number"){
      nums[x] = Math.floor(nums[x]);
      odds.push(nums[x])
    }
  }
  odds = odds.sort(function(a, b){return a-b});
  evens = evens.sort(function(a, b){return a-b});
  
  result = odds.concat(evens)
  return result;
}


//POWER USING RECURSION


function power(integer, exponent){
  let result = integer;
  if(exponent === 1){//Base case
    return result;
  }
  result *= power(integer, exponent-1);
  return result;
}


//LONGEST SENTENCE


function longest(sentence){
  let arr = sentence.split(" ").sort(function(a,b){return b.length - a.length});
  return arr[0];
}