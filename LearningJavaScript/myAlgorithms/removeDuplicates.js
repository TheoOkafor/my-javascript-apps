//This program removes special characters and duplicate alphabets from any word.

    test('removeDuplicates("menu") should return true', function(assert) {
      var result = removeDuplicates('menu'); // expect true
      var expected = "menu";
      assert.deepEqual(result, expected);
    });
    
    test('removeDuplicates("mama") should return ma', function(assert) {
      var result = removeDuplicates('mama'); // expect false
      var expected = "ma";
      assert.deepEqual(result, expected);
    });

    test('removeDuplicates("Mame") should return Mae', function(assert) {
      var result = removeDuplicates('Mame'); // expect false
      var expected = "Mae";
      assert.deepEqual(result, expected);
    });
    
    test('removeDuplicates("ebo0-=nyi123%") should return ebonyi', function(assert) {
      var result = removeDuplicates('ebo0-=nyi123%'); // expect ebonyi
      var expected = 'ebonyi';
      assert.deepEqual(result, expected);
    });

    function removeDuplicates(word){
      var alphabets = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];

      //This function removes letters that are English alphabets from the word.
      function removeSpecialChar(word){
        var arr = [];
        for (var x in word){
          if (alphabets.indexOf(word[x]) >= 0){//checks if an element in word is in aphabets
            arr.push(word[x]);
          }
        }
        return arr;
      }
      
      // This function takes the word and returns letters free from duplicates. 
      function removeDups(word){
        var arr = [];
        for (var x in word){
          if (arr.indexOf(word[x]) === -1){//checks if an element already in word is in arr
            arr.push(word[x]);
          }
        }
        return arr;
      }

      var noChar = removeSpecialChar(word)
      var cleanWord = removeDups(noChar);
      var result = cleanWord.join("");
      
      return result;
    }