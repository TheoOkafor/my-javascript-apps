
    test('sortStates(["Abia", "Anambra", "Ebonyi", "Enugu", "Imo"]) should return ["Imo", "Abia", "Enugu", "Ebonyi", "Anambra"]', function(assert) {
      
      var result = sortStates(["Abia", "Anambra", "Ebonyi", "Enugu", "Imo"]); // expect an array "expected"
      var expected = ["Imo", "Abia", "Enugu", "Ebonyi", "Anambra"];
      assert.deepEqual(result, expected);
    });

    test('sortStates(["Ekiti", "Lagos", "Ogun", "Ondo", "Osun", Oyo]) should return ["Oyo", "Ogun", "Ondo", "Osun", "Ekiti", "Lagos"]', function(assert) {
      var result = sortStates(["Ekiti", "Lagos", "Ogun", "Ondo", "Osun", "Oyo"]); // expect an array containing [50,20,10,5]
      var expected = ["Oyo", "Ogun", "Ondo", "Osun", "Ekiti", "Lagos"];
      assert.deepEqual(result, expected);
    });

    var allStates = [
      "Abia",
      "Adamawa",
      "Anambra",
      "Akwa Ibom",
      "Bauchi",
      "Bayelsa",
      "Benue",
      "Borno",
      "Cross River",
      "Delta",
      "Ebonyi",
      "Enugu",
      "Edo",
      "Ekiti",
      "FCT-Abuja",
      "Gombe",
      "Imo",
      "Jigawa",
      "Kaduna",
      "Kano",
      "Katsina",
      "Kebbi",
      "Kogi",
      "Kwara",
      "Lagos",
      "Nasarawa",
      "Niger",
      "Ogun",
      "Ondo",
      "Osun",
      "Oyo",
      "Plateau",
      "Rivers",
      "Sokoto",
      "Taraba",
      "Yobe",
      "Zamfara"
    ];

    test('sortStates(allStates)[0] should return "Edo"', function(assert) {
      var result = sortStates(allStates); // expect an array containing "expected".
      var expected = "Edo";
      assert.deepEqual(result[0], expected);
    });

/*Function sortStates sorts the 36 states by the length of the names and presents
the results in ascending order (shortest first).*/
//It's noteworthy that the algorithm used in the sort function is not scalable.
//the length of the shortest and longest names has to be known.

    function sortStates(state){
      var shortest = 3;
      var result = [];
      var i = shortest;
      while (i < 12){//11 is the length of the longest state in Nigeria
        for (var j = 0; j < state.length; j++) {//loops through the state array.
          if (state[j].length === i) {
            result.push(state[j]);
          }
        }
        i++; //increments to the next state.length
      }
      return result;
    }

//The algorithm/implementation below works, but it is not strict on the order/arrangement
    function ascSortStates(arr){
      arr.sort(function (a, b) { return a.length - b.length;//sorts the states in ascending order })
      // return b.length - a.length; to sort in descending order (Longest first).
      return arr;
    }
//Descending Order
    function descSortStates(arr){
      arr.sort(function (a, b) { return b.length - a.length ; })
      return arr;
    }