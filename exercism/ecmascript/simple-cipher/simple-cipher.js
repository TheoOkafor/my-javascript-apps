class Cipher{
	constructor(input){
		let key;
		if(input === null || input === undefined){
			key = this.randomKeyGen();
		}
		else if (this.isGoodKey(input) && input.length > 0){
			key = input;
		}
		else {
			throw "Bad key";
		}
		this.key = key;
		this.storage = {};
		this.storage2 = {};
	}

	isGoodKey(input) {
		let re = /[^a-z]/g;
		return input.match(re) === null ? true: false;
	}

	randomKeyGen() {//Generates a 100-letter key randomly.
		let i = 0;
		let alphabets = "qwertyuiopasdfghjklzxcvbnm".split("");
		let randomKey = [];
		function randomIndex(){
			return Math.floor(Math.random() * 25);
		}
		while (i < 100){
			randomKey.push(alphabets[randomIndex()]);
			i++;
		}
		return randomKey.join("");
	}

	encode(word) {
		let result;
		let j = word.length;
		let wordKey = this.key.substr(0, j);

		if (this.key.length >= 100){
			let j = word.length;
			let wordKey = this.key.substr(0, j);
			this.storage.wordKey = word;

			result = wordKey;
		}

		else{
			let anotherKey = [];
			if(wordKey.length < word.length){
				wordKey = this.extendKey(wordKey, word);
			}
			let i;
			for (i in word){
				anotherKey.push(this.shiftChar(wordKey[i], word[i]));
			}

			anotherKey = anotherKey.join("");
			this.storage2.anotherKey = word;

			result = anotherKey;
		}
		return result;
	}

	decode(wordKey){
		let result;
		if (this.key.length >= 100){
			 result = this.storage.wordKey;
		}
		else{
			result = this.storage2.anotherKey;
		}
		return result;
	}

	shiftChar(keyInput, wordInput){//Takes the key and the word, returns result
		let result;
		let alphabets = "qwertyuiopasdfghjklzxcvbnm".split("").sort();
		let minCharCode = 97; //Char Code of "a"
		let maxCharCode = 122;//Char Code of "z"

		let currentChar = wordInput.charCodeAt(0);
		let shiftIndex = alphabets.indexOf(keyInput);

		let shiftedCharCode = currentChar + shiftIndex;
	
		if(shiftedCharCode > maxCharCode){//Handles char Codes larger than maxCharCode.
			let reshift = shiftedCharCode - maxCharCode;
			shiftedCharCode = minCharCode + reshift - 1;
		}
		result = String.fromCharCode(shiftedCharCode);
		return result;
	}

	extendKey(keyInput, wordInput){//extends a short key and returns a key same length as word.
		let x = 0;
		keyInput = keyInput.split("");
		let newWordKey = [];

		while (x < wordInput.length){
			for(let i = 0; i < keyInput.length; i++){
           	if(wordInput.length !== newWordKey.length){
					newWordKey.push(keyInput[i]);
					x++;
           	}
			}
		}
      return newWordKey.join("");
	}
}

export default Cipher;