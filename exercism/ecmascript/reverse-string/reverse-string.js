/*function reverseString(word){
	let result;
	let wordArr = word.split("");//converts word to array
	result = wordArr.reverse().join("");//reverses the array and forms a string

	return result;
}*/


function reverseString(word){
	let result;
	let arr = word.split("");
	let container = [];
	for(let i = arr.length-1; i>=0; i--){
		container.push(arr[i]);
	}
	result = container.join("");

	return result;
}

export default reverseString;