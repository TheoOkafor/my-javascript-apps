class Pangram{
	constructor(word){
		this.word = word;
	}

	isEmpty(){
		let result = this.word === null || this.word === undefined || this.word === ""? true: false;
		return result;
	}

	removeDuplicates(word){
        var arr = [];
        for (var x in word){
          if (arr.indexOf(word[x]) === -1){//checks if an element already in word is in arr
            arr.push(word[x]);
          }
        }
        return arr;
     }

	isPangram(){
		let result;
		if(this.isEmpty()){
			result = false;
		}
		else{
			let re = /[a-z]/g;
			let alphabets = "abcdefghijklmnopqrstuvwxyz";
			let word = this.word.toLowerCase();
			word = word.match(re).sort();
			let freeWord = this.removeDuplicates(word).join("");
			result = freeWord === alphabets? true: false;
		}

		return result;
	}
}

export default Pangram;