class Transcriptor{

	singleDnaToRna(dna) {//Converts Each DNA element to RNA element.
		let result;
		switch(dna){
			case "G":
				result = "C";
				break;
			case "C":
				result = "G";
				break;
			case "T":
				result = "A";
				break;
			case "A":
				result = "U";
				break;
			default:
				throw "Invalid input DNA.";
		}
		return result;
	}

	toRna(dna){//Converts whole DNA to RNA.
			let rna = [...dna].map(this.singleDnaToRna);
			rna = rna.join("");
			return rna;
	}
}

export default Transcriptor;