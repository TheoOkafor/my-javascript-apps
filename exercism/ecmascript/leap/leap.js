class Year {
	constructor(year){
		this.year = year;
	}

	isLeap(){
		var result;
		if (this.year % 100 === 0 && this.year % 400 !== 0){
			result = false;
		}
		else if(this.year % 4 === 0){
			result = true;
		}
		return result;
	}
}

export default Year;